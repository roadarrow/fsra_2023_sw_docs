# Uputstvo za korišćenje Git Lab-a

**Napomena**: Zagrade <> stoje samo kao oznaka i ne treba ih koristiti u komandama
## Preuzimanje repozitorijuma
1. Napraviti nalog na GitLab sajtu koristeći mail drumske strele
2. Napraviti lokalno folder u kojem želite da vam se čuva projekat
3. Koristiti git bash alat
4. Kroz git bash doći do foldera koji ste napravili
    1. Komanda za kretanje kroz foldere je cd
    2. cd x: - komanda za odlazak na x particiju
    3. cd .. – komanda za odlazak u roditeljski direktorijum
5. Izvršiti sledeće komande za dovlačenje projekta sa GitLaba
    1. `$ git init`
    2. `$ git remote add origin <https link ka projektu sa GitLaba>`  **(Slika 1, dole)**
         * Nakon ovog koraka, pored linka, u plavim zagradama trebalo bi da vam se pojavi ime grane na kojoj trenutno radite
    3. `$ git pull <https link ka projektu sa GitLaba>`
## Pravljenje promena
Pre rada na nekoj funkcionalnosti, potrebno je premestiti se na odgovarajuću granu
za tu funkcionalnost
* Ukoliko je potrebno napraviti novu granu, to se radi na sledeći način:
    1. Na GitLab stranici projekta napraviti novu gradu sa dugmetom “New branch” **(Slika 2, dole)**
    2. Zatim kroz git bash, izvršiti komandu `$ git pull --all` , kako biste dohvatili sve promene koje su se desile na serveru
    3. `$ git checkout <ime grane>`, za prelazak na tu granu, u plavim zagradama trebalo bi da vam se pojavi ime grane na kojoj trenutno radite
* Ukoliko grana već postoji potrebno je samo kroz git bash izvršiti komande:
    1. `$ git pull --all`
    2. `$ git checkout <ime grane>`

Nakon napravljenih promena u projektu, treba te promene sačuvati i na GitLab, a za to služe sledeće komande
1. `$ git add`
2. `$ git commit -m "<poruka za commit>"`
3. `$ git push --set-upstream origin master`
    * Ovo je potrebno samo za prvi push, nakon toga dovoljno je izvršiti komandu git push
## Napomene
**Nikako push direktno na master granu!!!**  
+ Nakon što svoje promene sačuvate na grani, ne treba da radite push na master granu, ovo je posao koordinatora, a ono što vi treba da uradite jeste da napravite _Merge Request_ na
GitLab stranici projekta.
+ Za granu _from_ birate granu na kojoj ste upravo uradili push, a za granu _to_ birate **master**.
    + U opisu merge requesta potrebno je detaljnije opisati šta ste radili i menjali
    + U commit porukama treba da date kratke opise urađenog u tom commitu, najčešće jedna rečenica ili samo par reči.

Ispod su date slike za nekoliko koraka koje treba da radite na GitLab stranici projekta.
U slučaju bilo kakvih problema javite se prvi koordinatoru svog tima, pa će vas on upititi dalje
ako je to potrebno. U dokument će biti dodavana rešenja nekih čestih problema.
## Slike
Slika 1  
![Alt text](./Images/1.png?raw=true "1")  
Slika 2  
![Alt text](./Images/2.png?raw=true "1")