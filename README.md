# Drumska strela softver dokumentacija

Ideja je da ovaj projekat na Gitlabu posluži kao generalno mesto gde možemo čuvati neku generalnu dokumentaciju koja nije usko povezana za projekte. Dokumentaciju vezanu za projekte je dobro čuvati baš na repozitorijumu gde se nalazi i sam projekat

## Motivacija

**Pavle** --  Dok sam pokušavao da radim neke stvari malo sam se pogubio u nekoj dokumentaciji na drajvu jer nam folderi/fajlovi nisu baš najsrećnije imenovani. Tako da je ideja iza ovoga da se neke stvari prebace ovde i da generalno koristimo Gitlab malo bolje. Ideja je da sve što možemo održavamo ovde, dok ono što je prikladnije da održavamo na drajvu održavamo tamo a ovde eventualno kroz karatak README dokument opišemo i linkujemo dokument na drajvu. Takođe, u narednom periodu će se pisati dosta koda, pa je dobro da se usaglasimo oko načina na koji ćemo to raditi. Takođe, onboard novih članova.

## Kako koristiti ovaj projekat

Ovaj projekat je potrebno menjati kada se i/ili promene neke stvari vezane za već postojeće dokumenete koji se nalaze ovde ili ukoliko je potrebno dodati neki novi dokument koji je vezan za ceo softverski deo tima. Na primer

- Prva ste osoba u timu koja dodaje neku novu funkcionalnost vezanu za mikrokontroler - dodate dokument koji opisuje kako se to radi 
- Promenimo način na koji nešto radimo - neko iz tima promeni odgovarajući dokument na ovom projektu 
- Sve slično...

Što se tiče samog pisanja dokumenata ovde, mislim da je najpreglednije pisati **README** fajlove. Ništa nije specijalno teško, [ovaj tutorial](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax#links) bi trebalo da sadrži sve što je potrebno, ako se oko nečega ne snađete cimajte. Svakako, ideja nije da ovo nešto ulepšavamo, ali svakako je poželjno pisati što preglednije.

## Start
Za pocetak mozete procitati
- [Commits](workflow/commit.md)
- [PR](workflow/PR.md)
- [Code rules](code_rules/README.md)

## Podela uvodnog posla

Podela nekog uvodnog posla da prebacimo sto vise stvari ovde bi mogla da bude otprilike sledeca (manje vise svako sta je postavljao na drive)

- **Stefan** - [ADC cheatsheet](https://docs.google.com/document/d/1uL1fTdsYKF6Jg4nVmgp5ccospubPZJNW/edit)

Ako ostane jos nesto, dogovoricemo se oko toga. Generalno, svi saveti su dobrodosli

## TODO

U narednim danima/nedeljama treba ovo popuniti inicijalno što bolje, prebaciti pre svega uputstva za STM32 Cube što preglednije ovde.