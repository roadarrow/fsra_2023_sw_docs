# Define - konstante

Verovatno niste previše navikli da koristite define naredbu, ali moramo. Pogledajte stare kodove, tu se super koristi define naredba. U suštini, za nešto što ima neku relativnu random vrednost, za šta se ne može zaključiti iz koda šta predstavlja ide pod define

```
#define IME_OVAKO VREDNOST

#define ECU_GENERAL_STATE_CAN_ID 0x300
```