# Code rules

Ova sekcija predstavlja neka pravila koja želimo da poštujemo u svakom kodu na svakoj ploči. Nije nužno bitno da imamo neka striktna pravila, koliko je bitno da ih se svi pridržavamo.

## Napomene

Generalno ova pravila mozemo proizvoljno da se dogovorimo kako zelimo, najbitnije je da budemo konzistentni.

Ne počinjete promenljive i funkcije prefiksom HAL... , ali pretpostavljam da to nećete ni da radite.

## Formatiranje 

Za formatiranje koda koristicemo ugradjeni CUBE formater. Najbrzi nacin da ukljucite formater koji cemo koristiti.

```
Window > Preferences > C/C++ > Editor > Save Actions
```
Cekirate onda **Format source code** i **Format all lines**
