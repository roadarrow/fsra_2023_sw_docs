# Tipovi

Ukoliko definišete neke strukture, odnosno svoje tipove, imenujte ih na sledeći način

```
typedef struct ImeOvako{
    ...
}ImeOvako;

typedef struct CANMessage{
    int id;
    uint8_t data[...];
}CANMessage;
```
Ukoliko želite typedef da uradite na nekom prostom tipu

```
typedef tip1 typedef_ime_t;

typedef int can_id_t;
```