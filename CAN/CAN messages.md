# CAN poruke

Ideja je da grupisemo CAN poruke na osnovu toga koji podsistem (node) salje poruke. To radimo tako sto se u drugom bajtu nalazi ID podsistema, a u najnizem bajtu nalazi id poruke u okviru te grupe. Zbog toga je lakse i primati poruke u CoreLib-u. Manje prostora ce zauzimati bafer koji sluzi za primanje poruka.

```
  Byte 1    Byte 0
---------------------
|Group ID|Message ID|
---------------------
```
[Dokument o CAN porukama](https://docs.google.com/spreadsheets/d/1ziLLtcY7lbliwbYeEUBiFjdwSQXGUO53-eEt40v4KRM/edit?usp=drive_link)

## TODO

- popisati detaljno Orion poruke
- popisati LV poruke
- popisati strukturu svake poruke detaljno
- popisati koji podsistem je koji Group ID