# Bugs

Ovde cemo voditi dokumentaciju o bagovima koji su se javili u toku testiranja, kako biste znali da ih razresite brze ako se jave i vama, kao i onima koji su se potencijalno javili a da nismo umeli da ih razresimo

## Fixed bugs

### Boot pin

Primetili smo da MCU ne krene u izvrsavanje kada mu se dovede napajanje, vec je potrebno pritisnuti reset. Ovo nije ocekivano ponasanje jer u vozilu zelimo da kada okrenemo LVMS dovedemo napajanje na ploce, i tako odmah svaki MCU krene u izvrsavanje. 

Problem je bio u boot pinu na ploci, koji je bio postavljen na razdelnik 3V3 i GND, pa MCU nije citao logicku 0 na njemu koja je potrebna kako bi se pocetni program ucitao iz Flash memorije, kasnije se signal stabilizovao i davao 0 kada kliknemo reset. 

Resenje: Odlemiti pull up otpornik i samo ostaviti pull down na GND. Ovo je uradjeno po uzoru na NUCLEO semu

### CAN dioda

Dioda na CANH i CANL je bila lose orijentisana na semama, tako je i zalemljena pa je potrebno orijentisati je drugacije. Jos nekoliko stvari koje mozemo da razmotrimo na CAN semi na plocama su
- 100R na TX 
- 100R na RX
- kondenzator (da li treba) i kapacitivnost 

## Unfixed bugs


### STEF-STM CAN

Posto CAN na STEF-STMu mora da prodje optokaplar, a 5V i GND na izolovanoj strani nisu isti na dve strane optokaplara, tu se desava nesto cudno zbog cega ne mozemo da citamo i saljemo poruke kako treba. Elektronika debaguje to. Diode su dobro okrenute i sve ostalo je isto kao na ostalim semama

### ADC na senzorskoj

Probano je na sva tri načina čitanje sa tri kanala (sensor 3 - CH8, sensor 4 - CH9, sensor 5 - CH10) i svi su imali neke ofsete, čak i CH9 koji je par dana pre toga vraćao 0.
Žarko je odlemio sa senzora 3 sve otpornike i vezao direktno na mikrokontroler i onda nije bilo problema sa čitanjima.
Ovo ostavljamo još uvek pod "unfixed" pošto će se probati još neke stvari s tim razdelnicima dok se ne dođe do finalnog rešenja.