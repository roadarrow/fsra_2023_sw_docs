# Napajanje ploca

## Uvod

Pre nego sto predjemo na softverski deo price, potrebno je da znamo kako dovodimo napajanje na samu plocu. Dok se ploce ne stave u vozilo, potrebno je koristiti lab napajanje kako bismo to uradili. Elektronika ce izvuci zice za napajanje u vidu crvene (+) i crne (-) zice. Na njih je potrebno povezati + i - lab napajanja.

## Kako koristiti lab napajanje

Lab napajanje treba koristiti tako da prvo postavimo zeljeni maksimalno napon i struju. Za nase ploce treba postaviti napon na **12V** i struju na opseg **200mA - 1A**. Zasto opseg? Ne vuce svaka ploca istu struju, ali prilikom rada generalno ploce vuku **< 100mA**. Zasto onda postaviti gore navedeni opseg. Prilikom ukljucivanja napajanja MCU povuce malo vise struje (RUSH current), pa je zbog toga potrebno dozvoliti i malo vecu struju na pocetku. Pre ukljucivanja napajanja potrebno je ukljuciti **OCP** dugme na lab napajanju (overcurrent protection). Tek tada mozemo pritisnuti OUT dugme kako bismo dobili zeljeni napon i struju na izlazu. 

### Detaljnije uputstvo za lab napajanje

Laboratorisjko napajanje ima opciju za uključivanje Overcurrent protection - OCP, Overvoltage protection - OVP i memorisanje četiri različita podešavanja naponskog izvora.
Da bi se podesio napon ili maksimalna struja potrebno je kliknuti dugme **Voltage/Current**, cifra koja trepće se sada može podešavati okretanjem točkića **Adjust**.
Klikom na neko od tastera M1, M2, M3 i M4 se bira neki od već podešenih konfiguracija napajanja.

Postupak za uključivanje napajanja za potrebe napajanja ploča:
1. Kliknuti na okrugli taster **POWER** u donjem levom delu. Ono uključuje napajanje, ali napon još uvek nije prisutan na izlazu.
2. Pritisnuti dugme **OCP**, nakon toga je uključen overcurrent protection i svetli crvena lampica pored teksta OCP na displeju.
3. Proveriti da li je napon podešen na **12V** i da li je struja u **300mA** što je veće od očekivane *inrush* struje.
4. Pritiskom na **Off/On** taster se propoušta podešeno napajanje na izlaze crveni + i crni -. Zeleni GND ne koristiti.


Ocekivano ponasanje ploce je da diode koje proveravaju napon za 3.3V i 5V zasijaju kada dovedemo napajanje. Ovo cete svakako uglavnom raditi sa elektronicarima tako da ukoliko bude nekih problema oni bi trebalo da umeju da pomognu

Kada dovedemo napajanje na plocu, ona bi trebalo odmah da krene u izvrsavanje programa koji je u njenoj memoriji. Sledeci delovi dokumentacije se ticu toga kako treba isprogramirati sam MCU.

## Dalje

Citanje mozete nastaviti od [programmer.md](programmer.md)
