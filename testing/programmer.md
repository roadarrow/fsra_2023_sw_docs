# Programiranje MCU

Kada znamo kako dovodimo napajanje na plocu, potrebno je da vidimo kako treba isprogramirati plocu kada zelimo novi kod da uploadujemo na MCU. Ovo ste radili na NUCLEO plocama tako sto ste USB-om povezali laptop na plocu i uploadovali kod klikom na Run dugme u okviru CUBE okruzenja. U nastavku je opisano kako ovo otprilike funkcionise i sta treba uraditi sada kada zelimo da isprogramiramo nase ploce, a ne NUCLEO.

## Kako funkcionise programiranje ploce

Kada pogledate NUCLEO plocu, mozete videti da je ona izdeljena na dva dela. Gornji deo (programator) i donji deo (deo sa MCU cipom). Generalno, programator se moze koristiti u dva moda
- programiranje NUCLEO plocice
- programiranje nekog eksternog cipa

Po defaultu, kada dobijete NUCLEO, programator je spreman da isprogramira NUCLEO plocicu, zato vi ne morate nista da radite osim da kliknete Run kada povezete laptop na NUCLEO.

Drugi nacin kako programator moze da radi je programiranje eksternog MCU. To se radi preko SWD interfejsa, koji poseduje 6 pinova koji su izvuceni i na nasim plocama. Sta treba uraditi sa jumperima, koje pinove SWD-a povezati itd opisano je na sledecim linkovima 

```
https://os.mbed.com/questions/80077/Nucleo-5V-pin-only-show-27V-through-SWD/
https://stm32-base.org/guides/connecting-your-debugger.html
https://www.radioshuttle.de/en/turtle-en/nucleo-st-link-interface-en/
```

Kako ne bismo prepricavali ovde sta kazu linkovi. Napisacemo ispod samo najbitnija zapazanja.

## Najbitnije stvari prilikom programiranja cipa

Prva i najbitnija stvar prilikom programiranja cipa je - **RASKACITI LAB NAPAJANJE**. Ovo moramo da uradimo kako ne bismo dosli u situaciju da dovodimo 3V3 iz dva razlicita izvora sto moze napraviti problem.

Kada povezujete SWD pinove, najvisi pin (Vdd) iz nekog razloga ne dovodi 3v3, tako da mozete povezati ili 3V3 sa Nuclea ako je spojen programator sa NUCLEO-om ili sa JP1 desnog pina mozete povezati 3V3.

Kada je sve povezano, Run dugme bi trebalo da isprogramira MCU isto kao i kada programirate NUCLEO. Ukoliko zelite da testirate samo nesto sto je na 3V3 napajanju, ne morate vezivati opet 12V, ali svakako svako testiranje na kraju treba uraditi sa napajanjem od 12V.

## CoreLib testovi

Prva stvar koju treba pokrenuti na plocama su CoreLib testovi, koji su opisani u [corelib.md](corelib.md)