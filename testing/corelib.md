# CoreLib testovi

Pre testiranja bilo kakvih funkcionalnosti potrebno je pokrenuti CoreLib testove za periferije i testirati na osnovu tabele **Softver testiranje**

## UART test

UART test je zamisljen tako da ispisuje samo iteracije redom koristecti CoreLib API, tako se testira Transmit na UART-u. Receive UART-a se testira tako da kada preko hterm-a unesete 5 karaktera (bice dodata provera da su ti karakteri "reset"), ispisuje se poruke da se prijem desio. Prijem se desava u prekidu u CoreLib-u. Ovo je prvi test koji treba pokrenuti kako bi bismo posle imali ispis i za druge testove prilikom debagovanja.

## CAN test

CAN test je zamisljen kao ping pong razmena poruka izmedju dve ploce. Za to su dodate dve poruke u .dbc fajl
- TestMessage
- TestMessageNucleo 

Nasa ploca (ploca koja se testira) treba prva da posalje poruku, dok Nucleo ili neka druga ploca za koju znamo da CAN na njoj radi treba da odgovori i tako u krug. Na jednoj ploci treba pokrenuti deo koda kada je

```
#define BOARD 1
``` 

otkomentarisan, a na drugoj kada je zakomentarisan.

## ADC test

ADC test može vršiti sinhrono i asinhrono čitanje sa jednog kanala, kao i čitanje više kanala preko DMA. Za DMA ispratiti u ADC api dokumentaciji šta treba podesiti u ioc fajlu.