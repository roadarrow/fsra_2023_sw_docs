# Workflow

Ideja ovog foldera je da sadrži sva dokumenta koja opisuju način na koji želimo da radimo. Najbitnije stvari ovde će trenutno biti commit-ovi koje pravimo i nakon toga kada se završi celina pravljenje pull requesta.

## Osnove 

Generalno, nikada nemojte raditi na master/main grani. Kada želite da završite nešto, napravite granu kojoj možete davati imena u formatu

```
gitlab_username_what_i_do
```
na primer
```
pavle.janevski_CAN_API
```

Za sada najbitnije delovi ovde su [commit](commit.md) i [PR](PR.md) 

P.S. Znam da će doći period kada će ovo biti teško poštovati, ali bilo bi lepo da se potrudimo da što više ovo poštujemo kako se ne bismo gubili po sopstvenim projektima, a biće mnogo stvari da se odradi. 