# Commit

Commit je potrebno napraviti kada urasdite neki deo velike celine na kojoj radite ili kada samo želite da sačuvate nešto što ste radili u toku dana. Nekada možete otkucati puno koda koji nije kompletiran i skroz je normalno da napravite takav commit jer ukoliko kasnije imate previše vremena to može biti nezgodno i nepregledno.

## Generalno 

Commit poruke moraju biti što jasnije. Commit poruke pisati na engleskom. Poruka tipa 
```
Update
```
nikom ne objašnjava ništa, dok poruka
```
Changed way we receive messages 

- Async instead of sync 
- Added parameter for...
- Changed occurences of the function in the code
```
govori mnogo više. Tako da se trudite da u kratkim crtama u commit porukama kažete šta je u datom commitu urađeno. Moja ideja je da napravimo dve vrste commitova
- Možemo se vratiti na njih [Done]
- Ne možemo se vratiti na njih [In progress]

**Done** commit treba da predstavlja nešto na šta bismo mogli potencijalno da se vratimo prilikom testiranja npr, odnosno da znamo par stvari o tom kodu 
- Prevodi se
- Kompletiran u smislu da vi mislite da je to kompletna celina
- Nikakav problem ukoliko ne radi, ispravljamo sve

**In progress** commit predstavlja commit na koji znamo da ne možemo da se vratimo 
- Kod se ne prevodi (probajte da ovog svakako ne bude)
- Nije kompletno to što ste hteli (otkucano pola funkcije)
- Samo čuvate neki rad

U suštini potrebno da je commit samo u poruci obeležite, ako pogledamo primer odozgo

```
[In progress] Changed way we receive messages 

- Async instead of sync 
- Added parameter for...
- Changed occurences of the function in the code
```
Generalno, mozemo samo da obelezavamo **In progress** commitove, **Done** commit ne mora da sadrzi neku posebnu oznaku.

NOTE: Generalni stil pravljenja commit poruka je **(obratiti paznju na prazan red izmedju naslova i poruke)**

```
Title of the commit

As much text as you want...
```

## Sledeće

Sledeće što treba čitati je [PR](PR.md)

## TODO

Možda neki tutorial kako praviti commit iz različitih toolova
- Gitbash :)
- Github Desktop :(
