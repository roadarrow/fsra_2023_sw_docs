# PR 
BITNO JE DA SE TRUDIMO DA NIJEDNA PROMENA NA MASTER GRANI NE PROĐE BEZ PR-a.

PR (Pull Request) je potrebno napraviti kada kompletirate celinu na grani kojoj ste zamislili. Pre nego što uopšte pravite PR, uradite rebase u odnosu na master/main. 
Pod pretpostavkom da ste na svojoj grani, uradite komandu.
```
git rebase main
```
kao što piše na dnu dokumenta, biće napravljen detaljan tutorial da se ispetljate iz nezgodnih situacija ovde.

## Pravila za PR

PR se na Gitlabu zove **Merge request**, koraci kada odete na deo za Merge request
- **New merge request** dugme
- **Source branch** - vaša grana
- **Target branch** - uglavnom master, generalno gde želite da mergujete svoj kod
- Dugme **compare branches and continue**

Sada kada se nalazite na stranici za svoj merge request, potrebno je napisati naslov (ne mora biti previše detaljan). 

Nakon toga, potrebno je napisati poruku za Merge request, u kojoj treba detaljno opisati šta ste radili na toj grani i da li ste imali potencijalno neke probleme (svakako sve to treba propratiti i dokumentacijom). 

U polje Assigne stavite sebe.

U polje Reviewer stavite onog koji je glavni za tu plocu koju radite 
- BMS - Stefan
- Ekran - Luka
- Senzorska - Kristina
- ECU - Pavle
- CoreLib - Pavle 

Svakako, moja preporuka da link svakog PR-a pošaljete u grupu, kako bi barem dva čoveka pogledala PR. Na taj način, do neke mere ćemo svi ostati u toku sa onim što se dešava na svim pločama.

Za PR vam neko može zatražiti promene, to možemo preći na nekom sastanku, zatim nakon što ih ispravite ponovo zatražite review i ako je sve ok biće odobren PR.

## Napomena

Jako bitan deo workflow-a, može delovati zamorno na početku i teško ako niste raditi do sad tako. Bićemo zahvalni sebi kad dođe vreme za testiranje i kada treba brzo da se vraćamo na neke starije verzije koda potencijalno.

## TODO

Rebase može biti nezgodan, biće napravljen tutorial za to