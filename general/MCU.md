# Odabir MCU za ploce

Ove sezone (23/24) cemo koristiti sledece mikrokontrolere za nase ploce 

- BMS - [STM32F412VGT6](https://www.st.com/resource/en/datasheet/stm32f412cg.pdf)
- ECU - [STM32F412VGT6](https://www.st.com/resource/en/datasheet/stm32f412cg.pdf)
- Ekran - [STM32F469NI](https://www.st.com/resource/en/datasheet/stm32f469ae.pdf)
- Senzorske plocice - [STM32L431CCT6](https://www.digikey.com/en/products/detail/stmicroelectronics/STM32L431CCT6/6621814)

Narucene NUCLEO plocice - NUCLEO nije nuzno konkretan mikrokontroler, ali je dobra zamena sa slicnim periferijama, brzinom clock-a, memorijom...

- [STM32L431CCT6](https://eu.mouser.com/ProductDetail/STMicroelectronics/NUCLEO-F042K6?qs=kWQV1gtkNndvXZwikPmwPg%3D%3D) - 6 komada
- [STM32F412VGT6](https://estore.st.com/en/nucleo-f412zg-cpn.html) - 6 komada