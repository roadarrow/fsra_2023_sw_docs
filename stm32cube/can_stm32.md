# CAN podesavanje za STM32Cube
Podesavanja za clock
- System Core > RCC > High Speed Clock (HSE) - Crystal/Ceramic Resonator
- Nakon toga podesavanja u clock configuration

Podesavanja za sam CAN : U CubeMX
- Project Manager > Advanced Settings > Register Callbac > CAN - ENABLE 
- Connectivity > CANx
- Activated check


Prepostavimo da imamo predvidjeni Baud Rate koji zelimo. Treba da se zadovolji formula:  
```
Time for one bit = TQ + (TQ*TQ1) + (TQ*TQ2)

Time for one bit = 1 / Baud Rate
```

Prescaler se postavi tako da se za TQ (Time Quantum) dobije okrugla neka vrednost 
Time Quanta in Bit Segment 1 (TQ1) se postavi da se zadovolji formula
Time Quanta in Bit Segment 2 (TQ2) se postavi da se zadovolji formula

- NVIC Settings > CAN1 RX0 interrupt enabled se postavi

CAN u samom kodu (tipovi)
- CAN_HandleTypeDef - Uglavnom se polja popunjavaju na osnovi konfiguracije iz .ioc fajla 
- CAN_RxHeaderTypeDef - Zaglavlje za primanje poruka, Ne postavljaju mu se parametri u kodu. Prosledjuje se prilikom primanja poruke njegova adresa. Hardver postavlja vrednosti. Vrednosti se zatim citaju iz zaglavlja
- CAN_TxHeaderTypeDef - Zaglavlje za slanje poruka. Polja koja su bitna 
    - DLC - broj bajtova za podatke
    - IDE - da li koristimo 11 ili 29 bita za ID (tip identifikator) (vrednost CAN_STD_ID)
    - RTR - ?
    - StdId - ID u CAN poruci (ko salje poruku)
    - ExtId - ID u CAN poruci (ko salje poruku) extended verzija
    - TransmitGlobalTime - ?
- CAN_FilterTypeDef - Zaglavlje za CAN filter
    - FilterActivation - postavlja se na ENABLE da bi se aktivirao
    - FilterBank - koji se filter koristi (ima ih potencijalno vise)
    - FilterFIFOAssignment - za koji FIFO (0 ili 1) se postavlja filter
    - FilterMode - Mask ili List mode (vecu fleksibilnost daje Mask)

Postoje Mask i ID registar za prijem poruka

Tamo gde je bit postavljen na 1 u mask registru, ta poruka ce biti primljena samo ako je bit u poruci isti kao bit u ID registru

Tamo gde je bit postavljen na 0 u mask registru, ta poruka ce biti primljena i za vrednost 0 i 1 na tim mestima

**Primer**: za masku 0x7FE i ID registar 0x02 bice propusteni ID = 2 i ID = 3 jer bit na poziciji 0 u mask registru je 0, pa njega mozemo varirati, dok su ostali fiksirani pa se prepisuju iz ID registra
- FilterMaskIdHigh - visih 16 bita mask registra
- FilterMaskIdLow - nizih 16 bita mask registra
- FilterIdHigh - visih 16 bita ID registra

## CAN u samom kodu (funkcije)

-  **HAL_StatusTypeDef HAL_CAN_Start(CAN_HandleTypeDef \*hcan)** - startuje CAN modul (komunikaciju)

- **HAL_StatusTypeDef HAL_CAN_Init(CAN_HandleTypeDef \*hcan)** - inicijalizuje CAN periferiju na osnovu hcan atributa

- **HAL_StatusTypeDef HAL_CAN_ConfigFilter(CAN_HandleTypeDef \*hcan, const CAN_FilterTypeDef \*sFilterConfig)** - za datu CAN komunikaciju konfigurise odgovarajuci filter na osnovu njegovih postavljenih parametara

- **HAL_StatusTypeDef HAL_CAN_ActivateNotification(CAN_HandleTypeDef \*hcan, uint32_t ActiveITs)** - prvi atribut CAN modul, drugi atribut odgovara nekoj konstanti iz grupe CAN_interrupts koji govori kada ce se okinuti prekid, na primer konstanta CAN_IT_RX_FIFO0_MSG_PENDING okida prekid kada je pristigla poruka u FIFO0 bafer

- **HAL_StatusTypeDef HAL_CAN_AddTxMessage(CAN_HandleTypeDef \*hcan, const CAN_TxHeaderTypeDef \*pHeader, const uint8_t aData[], uint32_t \*pTxMailbox)** - salje zahtev za slanjem poruke, na osnovu zaglavlja i podataka formira paket i salje preko hcan komunikacije. Poslednji atribut je pokazivac na promenljivu u koju ce se upisati Mailbox(sanduce) u koju je postavljena poruka. Tacno znacenje mailbox-a?

- **HAL_StatusTypeDef HAL_CAN_GetRxMessage(CAN_HandleTypeDef \*hcan, uint32_t RxFifo, CAN_RxHeaderTypeDef \*pHeader, uint8_t aData[])** - primanje CAN poruke. Drugi parameter predstavlja bafer (CAN_RX_FIFO0) iz kojeg se prima poruka, dok je poslednji parametar niz u koji ce se smestiti bajtovi koji predstavljaju data deo poruke

- **HAL_StatusTypeDef HAL_CAN_RegisterCallback(CAN_HandleTypeDef \*hcan, HAL_CAN_CallbackIDTypeDef CallbackID, void (\* pCallback)(CAN_HandleTypeDef \*_hcan))** - funkcija koja postavlja callback funkciju za odredjeni prekid za datu CAN komunikaciju. Kao drugi parametar treba postaviti odgovarajucu konstantu koja odgovara prekidu koji zelimo da se desi. Kao treci parametar postavljamo funkciju proizvoljnog imena za koju zelimo da predstavlja nasu callback funkciju. 


## Linkovi

```
https://www.dailyduino.com/index.php/2020/06/01/stm32-can-bus/
https://github.com/MYaqoobEmbedded/STM32-Tutorials/tree/master/Tutorial%2033%20-%20CAN%20Bus
https://controllerstech.com/can-protocol-in-stm32/
https://matthewtran.dev/2020/01/setting-up-the-can-bus-on-stm32/
http://www.bittiming.can-wiki.info/
https://www.microchip.com/forums/m456043.aspx
https://schulz-m.github.io/2017/03/23/stm32-can-id-filter/
https://github.com/timsonater/stm32-CAN-bus-example-HAL-API/blob/master/main.c
```





