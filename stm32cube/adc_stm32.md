# ADC podesavanje za STM32Cube
## Podešavanja za ADC
- Categories > Analog > ADCx
- Izabrati kanal/kanale za prenos chekiranjem (pozabaviti se detaljima za biranje kanala)
- Proveriti na koje pinove je Cube postavio ulaze za ADC
- Podešavanje Clock Prescalera (kako?) i rezolucije ADCa
- NVIC Settings > Enabled check, ako je potrebno dozvoliti prekide za ADC
- DMA Settings > Add > Izabrati ADCx za koji zelimo da se prenosi pomoću DMA kontrolera ako je to potrebno

** Podešavanje DMA **

- Da bi se čitalo s više kanala u isto vreme preko DMA u Parameter Settings treba da bude namešteno sledeće:
    - Scan Conversion treba da bude Enabled, ali inicijalno nije moguće promeniti sa Disabled - ovo nije greška nego prvo mora da se stavi Number of Conversion parametar na veći od 1
    - Number of Conversion određuje sa koliko kanala će se vršiti čitanje
    - Pojaviće se odgovarajući broj Rank-ova gde se stavi na koji se se kanal odnosi i sampling time ako je potrebno promeniti

## ADC u kodu (tipovi)

- **ADC_InitTypeDef**
    - Polja se popunjavaju na osnovi .ioc fajla u fazi konfiguracije

- **ADC_ChannelConfTypeDef**
    - Struktura za konfiguraciju ADC kanala
    - Koristi se u fazi konfiguracije, ili u slučaju manualnog menjanja kanala za ADC konverziju
    - Polja koja su bitna
        - Channel - broj kanala, obavezno se navodi u formatu ADC_CHANNEL_X gde je X broj kanala, vrednosti ovih makroa definisane su pri stvaranju projekta
        - Rank - rank sekvencera (broj kanala za prenos?), obavezno se navodi u formatu ADC_REGULAR_RANK_X  gde je X broj ranka, obično 1
        - SamplingTime - vreme odabiranja kanala, navodi se broj ciklusa ADC timera i obavezno se navodi u formatu ADC_SAMPLETIME_XCYCLES, gde X mora da uzme diskretne vrednosti npr. 3, 15, 28… proveriti tačne vrednosti ovih makroa pre postavljanja

- **ADC_HandleTypeDef**
    - Handler za ADC, generisan u fazi konfiguracije od samog Cube-a za svaki ADC koji je konfigurisan, potrebno ga je koristiti na mnogim mestima gde se zahteva handler
    - Cube ga obično deklariše i konfiguriše sa imenom hadcx, gde je x broj ADC-a

## ADC u kodu (funkcije)

- **HAL_ADC_Init(ADC_HandleTypeDef \*hadc)**
    - Inicijalizuje ADC periferiju na osnovu hadc handlera
    - Zove je Cube u fazi konfiguracije

- **HAL_ADC_DeInit(ADC_HandleTypeDef \*hadc)**
    - Deinicijalizuje ADC periferiju i vraća registre za ovu periferiju u početno stanje

- **HAL_ADC_Start(ADC_HandleTypeDef \*hadc)**
    - Startuje ADC konverziju za ADC periferiju određenu na osnovu hadc handlera
    - Startuje ADC konverziju u blokirajućem modu (bez korišćenja DMA kontrolera)

- **HAL_ADC_Stop(ADC_HandleTypeDef \*hadc)**
    - Stopira ADC konverziju za ADC periferiju određenu na osnovu hadc handlera
    - Izbegavati korišćenje ove funkcije, obično nije potrebna

- **HAL_ADC_PollForConversion(ADC_HandleTypeDef \*hadc, uint32_t Timeout)**
    - Provera da li je konverzija završena koristeći Polling metodu
    - Parameteri
        - hadc - Handler za određenu ADC periferiju na kojoj je pokrenuta konverzija
        - Timeout - timeout vreme u milisekundama, ovo označava maksimalno vreme koje želimo da čekamo ako konverzija još uvek nije gotova
    - Polling metodu za ispitivanje kraja konverzije treba koristiti kada je konverzija započeta u blokirajućem modu tj. funkcijom HAL_ADC_Start
    - Povratna vrednost HAL_StatusTypeDef
        - Ukoliko nije gotova konverzija HAL_TIMEOUT
        - Ukoliko jeste HAL_OK

- **HAL_ADC_Start_IT(ADC_HandleTypeDef \*hadc)**
    - Startuje ADC konverziju za ADC periferiju određenu na osnovu hadc handlera
    - Startuje ADC konverziju u modu sa prekidom (bez korišćenja DMA kontrolera)
    - Za korišćenje ove funkcije moraju biti omogućeni prekidi za ADC u .ioc fajlu u delu NVIC Settings
    - Nakon startovanja konverzije ovom funkcijom, generiše se prekid u trenutku kada je konverzija gotova, i poziva se funckija HAL_ADC_IRQHandler koja predstavlja prekidnu rutinu za ovaj prekid, ova funkcija dalje poziva HAL_ADC_ConvCpltCallback funckiju koju mi treba da definišemo, dakle
        - Prekid za kraj konverzije > HAL_ADC_IRQHandler > HAL_ADC_ConvCpltCallback 

- **HAL_ADC_Stop_IT(ADC_HandleTypeDef \*hadc)**
    - Slično kao i HAL_ADC_Stop
    - Treba izbegavati ovu funckiju
    
- **HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef \*hadc)**
    - Funckija koja se zove na samom kraju konverzije startovane u modu sa prekidom
    - Ovu funkciju sa istim ovakvim potipisom potrebno je definisati na nekom mestu na našem fajlu sa ponašanjem kakvim želimo na kraju konverzije

- **HAL_ADC_Start_DMA(ADC_HandleTypeDef \*hadc, uint32_t \*pData, uint32_t Length)**
    - Startuje ADC konverziju i smešta rezultat u memoriju pomoću DMA kontrolera
    - Parametri
        - hadc - Handler za određenu ADC periferiju na kojoj pokrećemo konverziju
        - pData - adresa bafera za smeštanje podataka sa ADC periferije
        - Length - količina podataka koju DMA kontroler treba da prenese
    - Ovu funkciju moguće je koristiti samo ako je podešen DMA kontroler za određenu ADC periferiju u .ioc fajlu
    - Nakon kraja konverzije generiše se sličan prekid kao kada startujemo konverziju pomoću prekida

- **HAL_ADC_GetValue(ADC_HandleTypeDef \*hadc)**
    - Vraća rezultat konverzije za ADC periferiju određenu handlerom hadc
    - Funckija vraća rezulatat u formatu uint32_t
    - Funkciju je potrebno korisiti kada znamo da je konverzija završena metodom pollinga ili generisanim prekidom

Ovaj dokument služi kao podsetnik i početna tačka za rad sa ADC periferijama STM32 miktrokontrolera. 
Sve ovo detaljnije nalazi se u fajlovima stm32xx_had_adc.h i stm32xx_had_adc.c. Ovi fajlovi nalaze se u folderima Drivers > STM32_xx_HAL_Driver > inc i Drivers > STM32_xx_HAL_Driver > src.
